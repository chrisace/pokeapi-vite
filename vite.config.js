import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Pages from "vite-plugin-pages";
const path = require('path');

export default ({
  alias: {
    '@': path.resolve(__dirname, './src')
  },
  plugins: [
    vue({
      include: [/\.vue$/],
    }),
    Pages({
      extensions: ['vue'],
    }),
  ]
})
